alias docker-cleanup-images='docker rmi $(docker images -f "dangling=true" -q)'
alias docker-cleanup-containers='docker rm --volumes=true $(docker ps -a -q -f status=exited)'

export EDITOR=vim

# Shortcuts for directory listings
alias ll='ls -al'
alias lt='ls -altr'
alias la='ls -a'
# Hide snaps on recent versions of Ubuntu
alias df='df -x squashfs'
# Open multiple files specifed on command-line in their own tabs
alias vim='vim -p'
# Don't use old crap
alias vi='vim'
alias h='history'

# Timestamps with history -i
setopt extendedhistory
export HISTTIMEFORMAT='%Y-%m-%d %H:%M:%S  '
export HISTSIZE=1000

# Prevent ctrl-d logging me out
set -o ignoreeof

# NB: TERM must be set before sourcing of colours to ensure correct terminal
# escape codes
if [ ! -v TMUX ]; then
    # Only set this if not inside tmux
    TERM=xterm-256color
fi

if [ -e ~/.zsh.colours ]; then
    source ~/.zsh.colours
fi

function repo_branch() {
    git branch >/dev/null 2>/dev/null
    if (( $? == 0 )); then
        ref=$(git symbolic-ref HEAD | cut -d'/' -f3 | sed 's/\(.*\)/ (± \1)/') 2>/dev/null;
        echo "${green_fg}$ref${normal}"
        return
    fi
}
virtualenv_name() {
    if [ ! -z "$VIRTUAL_ENV" ] ; then
        name=`basename $VIRTUAL_ENV`
        if [ "$name" = "virtualenv" ]; then
            name=`dirname $VIRTUAL_ENV`
            name=`basename $name`
        fi
        echo " ${darkgray_fg}(∈ ${name})${normal}"
    fi
}
export VIRTUAL_ENV_DISABLE_PROMPT=1 # use our own virtualenv prompt instead of the default
user_colour() {
    if [[ `whoami` == "root" ]]; then
        echo -n "${red_fg}"
    else
        echo -n "${blue_fg}"
    fi
}
PROMPT=$'\n$(user_colour)'"%n@%m:${yellow_fg}%~${normal}"$'$(virtualenv_name)$(repo_branch)'$'\n'"%% "
setopt prompt_subst

if [ -f ~/.zshrc.local ]; then
    # All stuff specific to the local environment
    source ~/.zshrc.local
fi
