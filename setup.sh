#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
home_linkables='.tmux.conf .vimrc .vim .gitconfig .zshrc .zsh.colours .zprofile'

echo "Setting up links"
for linkable in $home_linkables; do
    dest=$HOME/$linkable
    if [ -e $dest ]; then
        echo "$dest already exists";
    else
        echo "Linking $dest to $DIR/$linkable"
        ln -s "$DIR/$linkable" $dest
    fi
done

config_linkables='nvim'
for linkable in $config_linkables; do
    dest=$HOME/.config/$linkable
    if [ -e $dest ]; then
        echo "$dest already exists";
    else
        echo "Linking $dest to $DIR/$linkable"
        ln -s "$DIR/$linkable" $dest
    fi
done
