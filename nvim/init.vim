" Requires vim-plug at ~/.local/share/nvim/site/autoload/plug.vim
" See https://github.com/junegunn/vim-plug
call plug#begin(stdpath('data') . '/plugged')

  Plug 'mbbill/undotree'
  Plug 'junegunn/vim-easy-align'

call plug#end()

" Undo - https://github.com/mbbill/undotree
if has("persistent_undo")
  set undodir="~/.undodir"
  set undofile
endif
nnoremap <F5> :UndotreeToggle<cr>

" Easy-align - https://github.com/junegunn/vim-easy-align
xmap ga <Plug>(EasyAlign)
nmap ga <Plug>(EasyAlign)
