let g:colors_name = 'discoloured'

highlight clear Normal
set background&
highlight clear

if &background == "light"

    " Search highlighting
    highlight Search ctermbg=Green ctermfg=Black guibg=Green guifg=Black
    " Make it more obvious which window we are currently in
    highlight StatusLine ctermfg=Blue ctermbg=White
    highlight StatusLineNC ctermfg=Gray ctermbg=Black
    " Make nicer looking tabline
    highlight TabLineSel ctermbg=Blue ctermfg=White
    " Diffs
    "hi DiffChange	guibg=darkGreen		guifg=black	ctermbg=darkGreen	ctermfg=black
    highlight DiffText		guibg=yellow		guifg=black		ctermbg=yellow	ctermfg=black
    highlight DiffAdd		guibg=green		guifg=black		ctermbg=green		ctermfg=black
    highlight DiffDelete   guibg=red			guifg=black	ctermbg=red		ctermfg=black

    " LHS line number column
    highlight LineNr term=underline ctermfg=3 guifg=Brown guibg=LightGray
    " Spelling mistakes
    highlight SpellBad term=reverse ctermbg=1 gui=undercurl guisp=Red

    " Popups
    highlight Pmenu guibg=blue guifg=black ctermbg=blue ctermfg=black
    highlight PmenuSel guibg=darkblue guifg=white ctermbg=darkblue ctermfg=white
    highlight PmenuSbar guibg=darkgray guifg=lightgray ctermbg=darkgray ctermfg=lightgray

    highlight MatchParen gui=bold guibg=NONE guifg=lightblue cterm=bold ctermfg=lightblue ctermbg=NONE

    highlight Label guibg=NONE guifg=DarkGreen ctermbg=NONE ctermfg=DarkGreen

    "~~ Custom Syntax Groups ~~"

    highlight Error ctermfg=White ctermbg=Red guifg=White guibg=Red
else

    highlight Define ctermfg=Magenta
    highlight String ctermfg=Red
    highlight Comment ctermfg=DarkBlue
    highlight PreProc ctermfg=Magenta
    highlight PreCondit ctermfg=Blue
    highlight Exception ctermfg=Red
    highlight Function ctermfg=LightGreen

    " Search highlighting
    highlight Search ctermbg=Green ctermfg=Black guibg=Green guifg=Black
    " Make it more obvious which window we are currently in
    highlight StatusLine ctermfg=Blue ctermbg=White
    highlight StatusLineNC ctermfg=Gray ctermbg=Black
    " Make nicer looking tabline
    highlight TabLineSel ctermbg=Blue ctermfg=White
    " Diffs
    "hi DiffChange	guibg=darkGreen		guifg=black	ctermbg=darkGreen	ctermfg=black
    highlight DiffText		guibg=yellow		guifg=black		ctermbg=yellow	ctermfg=black
    highlight DiffAdd		guibg=green		guifg=black		ctermbg=green		ctermfg=black
    highlight DiffDelete   guibg=red			guifg=black	ctermbg=red		ctermfg=black

    " Popups
    highlight Pmenu guibg=blue guifg=black ctermbg=blue ctermfg=black
    highlight PmenuSel guibg=darkblue guifg=white ctermbg=darkblue ctermfg=white
    highlight PmenuSbar guibg=darkgray guifg=lightgray ctermbg=darkgray ctermfg=lightgray

    highlight MatchParen gui=bold guibg=NONE guifg=lightblue cterm=bold ctermfg=lightblue ctermbg=NONE

    highlight Label guibg=NONE guifg=DarkGreen ctermbg=NONE ctermfg=DarkGreen

    "~~ Custom Syntax Groups ~~"

    highlight Error ctermfg=White ctermbg=Red guifg=White guibg=Red
endif

" TODO: Others to determine (see :help hl)

" Cursor line
"highlight CursorLine guibg=#333333
"highlight CursorColumn guibg=#333333

" Omni menu
"highlight Pmenu guibg=#333333
"highlight PmenuSel guibg=#333333
