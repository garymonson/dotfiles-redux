" Number of spaces a tab counts for
set tabstop=2
" Number of spaces a tab counts for in editing operations such as pressing
" <Tab> or <BS>
set softtabstop=2
" Number of spaces to use for each step of (auto)indent.  Used for 'cindent',
" >>, <<, etc
set shiftwidth=2
