"------------------------------ Typos --------------------------------"
    nmap :1 :!
    nmap ; :
"---------------------------------------------------------------------"


"------------------------- Display Configuration ---------------------"
    " Colours for dark backgrounds
    set background=dark
    " 256 colours if terminal allows it (gnu screen doesn't, and it looks like crap)
    set t_Co=256
    " Show syntax highlighting, etc
    syntax on
    " Show ruler at bottom of screen
    set ruler
    " Show line numbers on LHS
    set number
    " Use cursorline while in insert mode, to allow you to see the cursor
    " location more easily
    autocmd InsertEnter * set cursorline
    autocmd InsertLeave * set nocursorline
    " Custom status line
    set statusline=%F\ %m\ [fmt=%{&fileformat}]\ [enc=%{&fileencoding}]\ [type=%{&filetype}]%=(col\ %c,\ line\ %l)\ of\ %L\ lines\ %h\ (%P)
    " Status line always on
    set laststatus=2
    " Put filename into xterm title
    set title
    set encoding=utf-8
    " Show tabs and trailing whitespace specially
    " Lines longer than the terminal width are indicated by '<' and '>'
    set list listchars=tab:▸\ ,extends:❯,precedes:❮,trail:_
    set showbreak=↪
    colorscheme discoloured
"---------------------------------------------------------------------"


"------------------------- Search Behaviour --------------------------"
    " Highlight search matches
    set hlsearch
    " Shortcut to unhighlight last search
    nnoremap <leader><space> :nohlsearch<Return>
    " Start searching immediately - even before <Enter> is pressed
    set incsearch
    " C-l to toggle search highlighting
    nnoremap <C-l> :set hlsearch!<CR><C-l>
    if executable('ag')
        set grepprg=ag\ -a
    elseif executable('ack')
        set grepprg=ack\ --nogroup\ -H
    endif
"---------------------------------------------------------------------"


"------------------------- Editing Behaviour -------------------------"
    " Use filetype indenting plugins
    filetype plugin indent on
    " Preserve indent level from previous line to new line
    set autoindent
    " Number of spaces a tab counts for
    set tabstop=4
    " Number of spaces a tab counts for in editing operations such as pressing
    " <Tab> or <BS>
    set softtabstop=4
    " Number of spaces to use for each step of (auto)indent.  Used for 'cindent',
    " >>, <<, etc
    set shiftwidth=4
    " <Tab> at start of line inserts &shiftwidth spaces, <BS> deletes a
    " &shiftwidth worth of spaces at start of line
    set smarttab
    " Automatic indenting where appropriate
    set smartindent
    " Make searches case-insensitive
    " (NB: If you want a case-sensitive search on the fly, prepend your search
    " with \C)
    set ignorecase
    " ...unless you include at least one capital letter in your search
    set smartcase
    " Pressing Tab inserts spaces
    set expandtab
    " Extra backspacing power:
    " indent    - allow backspacing over autoindent
    " eol       - allow backspacing over line breaks
    " start     - allow backspacing over start of insert; CTRL-W and CTRL-U stop
    "             once at start of insert
    set backspace=indent,eol,start
    " Opening brackets are briefly shown when entering closing brackets
    set showmatch
    " Time opening brackets are shown thanks to showmatch is 3/10 s
    set matchtime=3
    " Keep at least 5 lines above or below cursor in window
    set scrolloff=5
    " Set maximum line width of 80 chars (breaks lines if they pass this point)
    set textwidth=0
    " Behaviour for selection:
    " * unnamed: Use the clipboard register '*' for all yank, delete, change and put
    " operations which would normally go to the unnamed register.  This means Vim
    " uses the Xll clipboard.  Does not work for remote terminals, of course.
    " * autoselect: Text selected with visual mode is used in the windowing system's
    " global selection (for middle-click pasting in X11, for example).
    " * exclude: Don't connect to X server (for above behaviour) if &term matches
    " this pattern.
    set clipboard=unnamed,autoselect,exclude:cons\|linux\|xterm\|rxvt
    " Ignore files you'd never want to edit, on tab-autocomplete
    set wildignore=*CVS/,*.o,*.pyc,*.bak,*.BAK,*.gsm,.svn/,.git/,*.DS_Store
    " Show available completion options as selection is made
    set wildmenu
    " When switching buffers, check if the buffer is in another tab first
    set switchbuf=usetab
    " Use popup menu for completions, and show extra info about current selection in
    " preview window
    set completeopt=menu,preview
    " TODO: Check out completefunc and omnifunc and ins-completion for more info
    " Backup current file, delete afterwards
    set nobackup
    set writebackup
    " Longer command history
    set history=1000
    " Tab/Shift-Tab for indent/dedent visually selected text, and stay selected
    " afterwards
    xmap <Tab> >'<V'>
    xmap <S-Tab> <'<V'>
    " Prevent # being moved to first column (see :help smartindent)
    inoremap # X#
"---------------------------------------------------------------------"


"----------------------- Command-line Shortcuts ----------------------"
    " C-a to start-of-line (like readline)
    cnoremap <C-a> <Home>
"---------------------------------------------------------------------"


"------------------------- Tabline Navigation ------------------------"
    " We want lots of tabs!!
    set tabpagemax=60
    " Navigation between tabs
    nmap <silent> z<Right>   :tabnext<Return>
    nmap <silent> z<Left>    :tabprev<Return>
    nmap <silent> z<Up>      :tabfirst<Return>
    nmap <silent> z<Down>    :tablast<Return>
    nmap <silent> z<Delete>  :tabclose<Return>
    " Alternative for where Delete is not available or comfortable
    nmap <silent> z<BS>      :tabclose<Return>
    " Move the current tab to the start of the tab list
    nmap <silent> Z<Up>      :tabmove 0<Return>
    " Move the current tab to the end of the tab list
    nmap <silent> Z<Down>    :execute "tabmove ".tabpagenr('$')<Return>
    " Move current tab right one position in tab list
    nmap <silent> Z<Right>   :execute "tabmove ".tabpagenr()<Return>
    " Move current tab left one position in tab list
    nmap <silent> Z<Left>    :execute "tabmove ".(tabpagenr() - 2)<Return>
    if has("gui")
        " Control tabs like Firefox etc
        nmap <silent> <C-Tab>    :tabnext<Return>
        nmap <silent> <C-S-Tab>  :tabprev<Return>
    endif
"---------------------------------------------------------------------"


"------------------------- Window Navigation -------------------------"
    " W steps to next window
    nmap W <C-w>w
"---------------------------------------------------------------------"
